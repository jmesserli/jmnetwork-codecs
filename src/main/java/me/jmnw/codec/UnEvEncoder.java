/*
 § Copyright (c) 2015 Joel Messerli - JMNetwork.ch / JMNW.me
 §
 § UnEvEncoder.java is part of JMNW Codecs
 §
 § JMNW Codecs is free software: you can redistribute it and/or modify
 § it under the terms of the GNU General Public License as published by
 § the Free Software Foundation, either version 3 of the License, or
 § (at your option) any later version.
 §
 § JMNW Codecs is distributed in the hope that it will be useful,
 § but WITHOUT ANY WARRANTY; without even the implied warranty of
 § MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 § GNU General Public License for more details.
 §
 § You should have received a copy of the GNU General Public License
 § along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jmnw.codec;

import java.util.ArrayList;
import java.util.stream.IntStream;

/**
 * Joel Messerli am 14.03.2015.
 * www.jmnetwork.ch
 */
public class UnEvEncoder {

    public static String multiEncode(String s, int times) {
        String temp = s;
        for (int i = 0; i < times; i++) temp = encode(temp);
        return temp;
    }

    public static String encode(String s) {
        return encode(s, false);
    }

    public static String encode(String s, boolean info) {
        char[] input = s.toCharArray();
        char[] encoded = new char[s.length()];
        int[] indexes = generateIndexes(s.length());

        for (int i = 0; i < indexes.length; i++) {
            encoded[indexes[i]] = input[i];
            if (info) System.out.printf("%s: %3d => %3d (%s)%n", input[i], i, indexes[i], new String(encoded));
        }

        return new String(encoded);
    }

    public static String multiDecode(String s, int times) {
        String temp = s;
        for (int i = 0; i < times; i++) temp = decode(temp);
        return temp;
    }

    public static String decode(String s) {
        return decode(s, false);
    }

    public static String decode(String s, boolean info) {
        char[] input = s.toCharArray();
        char[] decoded = new char[s.length()];
        int[] indexes = generateIndexes(s.length());

        for (int i = 0; i < s.length(); i++) {
            decoded[i] = input[indexes[i]];
            if (info) System.out.printf("%s: %3d => %3d (%s)%n", input[i], indexes[i], i, new String(decoded));
        }

        return new String(decoded);
    }

    public static String getDecodingKey(int length, int times) {
        String initial = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        if (length > initial.length()) throw new IllegalArgumentException("length must be <= " + initial.length());

        initial = initial.substring(0, length);

        return multiEncode(initial, times);
    }

    public static ArrayList<Integer> findCollisions(String s, int collisionCount) {
        ArrayList<Integer> collisions = new ArrayList<>();
        int i = 1;
        while (collisions.size() < collisionCount) {
            String encoded = multiEncode(s, i);
            if (encoded.equals(s)) collisions.add(i);
            i++;
        }

        return collisions;
    }

    public static int[] generateIndexes(int arraySize) {
        ArrayList<Integer> tempList = new ArrayList<>();
        int[] tempArray = new int[arraySize];

        IntStream.range(0, arraySize).filter(i -> i % 2 == 1).forEach(tempList::add);
        IntStream.range(0, arraySize).filter(i -> i % 2 == 0).forEach(tempList::add);
        IntStream.range(0, arraySize).forEach(i -> tempArray[i] = tempList.get(i));

        return tempArray;
    }

}