/*
 § Copyright (c) 2015 Joel Messerli - JMNetwork.ch / JMNW.me
 §
 § Main.java is part of JMNW Codecs
 §
 § JMNW Codecs is free software: you can redistribute it and/or modify
 § it under the terms of the GNU General Public License as published by
 § the Free Software Foundation, either version 3 of the License, or
 § (at your option) any later version.
 §
 § JMNW Codecs is distributed in the hope that it will be useful,
 § but WITHOUT ANY WARRANTY; without even the implied warranty of
 § MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 § GNU General Public License for more details.
 §
 § You should have received a copy of the GNU General Public License
 § along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jmnw.codec;

import java.util.ArrayList;

/**
 * Joel Messerli am 14.03.2015.
 * www.jmnetwork.ch
 */
public class Main {
    public static void main(String[] args) {
        String testString = "aBcDeFgHiJkLmNoPqRsTuVwXyZ";
        int times = 9;

        long startEncoding = System.nanoTime();
        String encoded = UnEvEncoder.multiEncode(testString, times);
        long endEncoding = System.nanoTime();
        long startDecoding = System.nanoTime();
        String decoded = UnEvEncoder.multiDecode(encoded, times);
        long endDecoding = System.nanoTime();

        String decodingKey = "Generation failed";
        try {
            decodingKey = UnEvEncoder.getDecodingKey(testString.length(), times);
        } catch (IllegalArgumentException e) {
            System.out.println("DecodingKey could not be generated because of: " + e.getMessage());
        }
        ArrayList<Integer> collisions = UnEvEncoder.findCollisions(testString, 20);

        System.out.printf("%s\nEncoded (%sx): <%s> (%s ms)\nDecoded (%sx): <%s> (%s ms)%n", testString, times, encoded, (endEncoding - startEncoding) / 1E6D, times, decoded, (endDecoding - startDecoding) / 1E6D);
        System.out.printf("Key: %s%n", decodingKey);

        System.out.println(collisions);
    }
}
